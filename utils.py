import subprocess
import glob
import os

def flatten_shape(shape):
    length = 1
    for s in shape:
        length *= s
    return length


def get_revision_hash():
    '''
    Returns the git revision hash of this project.
    :return: str: git revision hash or None if not found
    '''
    try:
        git_output = str(subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']), 'utf-8').strip()
        return git_output
    except subprocess.CalledProcessError:
        return None


def should_stop():
    '''
    For repeated trainings, such as metasearch, interrupts the loop if a STOP file is found in the project directory.
    :return:
    '''
    return os.path.exists('./STOP')
