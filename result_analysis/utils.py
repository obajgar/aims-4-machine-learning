import collections
import pandas as pd

def extract_df(results, cols, index='tot_iter'):
    """
    Extracts a pandas DataFrame from a list of results, where each item
    is a dict containing an entry for "index" and possibly one or multiple
    "cols".
    :param results: list(dict)
    :param cols: list(str) - list of keys to extract from results
    :param index: str - index
    :return:
    """
    index_dict = collections.defaultdict(lambda: {})
    for result in results:
        for k, v in result.items():
            if k in cols:
                index_dict[result[index]][k] = result[k]

    res_df = pd.DataFrame.from_dict(index_dict, orient='index')

    return res_df

from ord.analysis.ord2dataframe import ord2dataframe