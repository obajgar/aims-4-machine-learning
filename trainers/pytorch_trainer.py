from trainers.trainer_base import TrainerBase
import torch

class PytorchTrainer(TrainerBase):

    def calculate_gradients(self, loss):
        loss.backward()
        gradients = [w.grad for w in self._model.params]
        return gradients

    def sanitize(self, x):
        return torch.tensor(x)