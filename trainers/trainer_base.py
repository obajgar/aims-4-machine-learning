from configuration import configuration, ConfigurableObject
from utils import get_revision_hash
from config.ord_config import ord_config
import time


class TrainerBase(ConfigurableObject):
    """
    Base class handling the training and evaluation process. This class should be kept independent of
    backends like Torch or Tensorflow to be compatible with multiple backends.
    """

    def __init__(self, model, loss, train_dataset, optimizer, extra_evaluators=(), valid_dataset=None,
                 test_dataset=None, long_name='trainer', short_name='t', enable_comet=False):
        """

        :param model: ModelBase - model to be trained and evaluated
        :param loss: EvaluatorBase - primary training loss
        :param train_dataset: DatasetBase
        :param optimizer: OptimizerBase - handles parameter updates
        :param extra_evaluators: list(EvaluatorBase) - list of evaluators evaluated at each epoch but not used for training
        :param valid_dataset: (optional) DatasetBase validation dataset
        :param test_dataset: (optional) DatasetBase test dataset
        :param long_name: long name for the purposes of handling experiment parameters
        :param short_name: short_name for
        :param enable_comet: enable logging in CometML
        """
        super().__init__(long_name, short_name)

        self.add_parameter('epochs', 'e', type=int, default=10,
                           description='Terminate training after e epochs.')
        self.add_parameter('enable_comet', 'ec', type=bool, default=enable_comet,
                           description='Enables logging to Comet ML .')
        self.add_parameter('disable_ord', 'ord', type=bool, default=False,
                           description='Enables logging to Comet ML .')

        self.add_child(optimizer)

        self._model = model
        self._train_data = train_dataset
        self._valid_data = valid_dataset
        self._test_data = test_dataset
        self._loss = loss
        self._extra_evaluators = extra_evaluators
        self._all_evaluators = [loss] + extra_evaluators

        self._epoch = 0
        self._iteration = 0
        self._total_iterations = 0

        # To be initialized in self.initialize()
        self._experiment_id = None
        self.ord = None
        self._comet_ex = None
        self._last_log_time = time.time()

    def initialize(self):

        super().initialize()

        self._experiment_id = self.experiment_id()
        if not self['disable_ord']:
            self._initialize_ord()
        if self['enable_comet']:
            from comet_ml import Experiment as CometExp
            from config.comet_config import comet_conf
            self._comet_ex = CometExp(**comet_conf)
            self._comet_ex.log_parameters(self.param_dict)
            self._comet_ex.set_name(self._experiment_id)

        print(f"Experiment id: {self._experiment_id}")



    def run_training(self):
        configuration.parse_arguments()
        configuration.initialize()

        tot_params = self._model.total_params
        if not self['disable_ord']:
            self.ord.update({'metadata': {'model_params': tot_params}})
        print(f"Initialization complete. # model parameters: {tot_params}")

        # Pre-training evaluation:
        if self._valid_data:
            self.evaluate(self._valid_data, 'val')
        if self._test_data:
            self.evaluate(self._test_data, 'test')

        # Main training loop:
        for epoch in range(self['epochs']):
            self.run_epoch()

        if not self['disable_ord']:
            self.ord.flush()

    def run_epoch(self):

        self._train_data.epoch_reset()
        self._epoch += 1
        self._iteration = 0

        for x_b, y_b in self._train_data.get_epoch_iterator():

            self._iteration += 1
            self._total_iterations += 1

            x_b = self.sanitize(x_b)
            y_b = self.sanitize(y_b)

            y_pred_b = self._model(x_b)
            loss = self._loss.partial_eval(y_b, y_pred_b, weights=self._model.params)

            for evaluator in self._extra_evaluators:
                evaluator.partial_eval(y_b, y_pred_b, weights=self._model.params)

            gradients = self.calculate_gradients(loss)
            self.opt.step(self._model, gradients)

        # Log mean values of evaluations on the training dataset collected during training:
        self.log_result({'train_' + evaluator.name: evaluator.report_mean()
                         for evaluator in self._all_evaluators})

        # End-of-epoch evaluation:
        if self._valid_data:
            self.evaluate(self._valid_data, 'val')
        if self._test_data:
            self.evaluate(self._test_data, 'test')

    def calculate_gradients(self, loss):
        raise NotImplementedError("Needs to be implemented.")

    def evaluate(self, dataset, prefix=''):
        '''
        Evaluates the model on the dataset.
        :param dataset: object with a .get_epoch_iterator() which yields an
                        iterator over pairs of x,y batches
        :return: None
        '''

        if prefix and prefix[-1] != '_':
            prefix += '_'

        for x_b, y_b in dataset.get_epoch_iterator():
            x_b = self.sanitize(x_b)
            y_b = self.sanitize(y_b)

            y_pred_b = self._model(x_b)
            for evaluator in self._all_evaluators:
                evaluator.partial_eval(y_b, y_pred_b, weights=self._model.params)

        self.log_result({prefix+evaluator.name: evaluator.report_mean() for evaluator in self._all_evaluators})

    def log_result(self, result):
        if self._comet_ex:
            self._comet_ex.log_metrics(result, epoch=self._epoch)
        result['epoch'] = self._epoch
        result['iteration'] = self._iteration
        result['tot_iter'] = self._total_iterations
        print(result)
        new_time = time.time()
        print(f"Time since last result: {new_time-self._last_log_time} s")
        self._last_log_time = new_time

        if not self['disable_ord']:
            self.ord.append_result(result)

    def experiment_id(self):
        trainer_name = self.__class__.__name__
        model_name = self._model.__class__.__name__
        env_name = self._train_data.__class__.__name__

        timestamp = time.strftime('%y%m%d_%H%M')

        _id = '_'.join([trainer_name, model_name, env_name, configuration.param_id_string(), timestamp])

        return _id

    def _initialize_ord(self):
        # Connection to Ondrej's result database - just stuff for logging
        from ord import OrdConnection
        self.ord = OrdConnection(**ord_config)

        repo = 'gitlab.com/obajgar/aims-4-machine-learning'
        revision_hash = get_revision_hash()

        self.ord.update({'_id': self._experiment_id,
                           'metadata': {
                               'repository': repo,
                               'revision_hash': revision_hash,
                               'metric_names': self.all_metric_names()
                           },
                           'configuration':
                               configuration.param_dict
                         })

    def all_metric_names(self):
        datasets = ['train']
        if self._valid_data:
            datasets.append('val')
        if self._test_data:
            datasets.append('test')

        metric_names = []
        for ev in self._all_evaluators:
            for d in datasets:
                metric_names.append(d+'_'+ev.name)

        return metric_names

    def sanitize(self, x):
        """

        :param x: any data
        :return: sanitized data (typically conversion into appropriate tensor type)
        """
        return x

    @property
    def current_epoch(self):
        return self._epoch

    @property
    def results(self):
        return self.ord._tmp_result['results']