from configuration import ConfigurableObject


class DatasetBase(ConfigurableObject):
    """
    Base class for dataset objects. Should implement a get_epoch_iterator() method.
    """

    def __init__(self, long_name='dataset', short_name=None):
        super().__init__(long_name=long_name, short_name=short_name)
        self.add_parameter('batch_size', 'b', type=int, default=1024)

        self._n_examples = None

    def epoch_reset(self):
        pass

    def get_epoch_iterator(self):
        raise NotImplementedError("Need to implement epoch reset method.")

    @property
    def n_examples(self):
        return self._n_examples
