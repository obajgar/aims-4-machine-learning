from datasets.dataset_base import DatasetBase
import torch

class SyntheticDataset(DatasetBase):

    def __init__(self, force_coefficient=None, dims = (1,1), **kwargs):
        super().__init__(**kwargs)
        self.add_parameter('noise_sd', type=float, default=0.1)
        self.add_parameter('n', type=int, default=20)

        self._dims = dims

        self.x_data = None
        self.y_data = None
        self.coeff = force_coefficient

    def initialize(self):
        if self.coeff is None:
            self.coeff = 5.*torch.randn(self._dims)

        noise = self['noise_sd'] * torch.randn(self['n'], self._dims[1], 1)
        self.x_data = torch.rand((self['n'], self._dims[0], 1))
        self.y_data = self.coeff @ self.x_data + noise



    def get_epoch_iterator(self, shuffle=True):
        new_order = torch.randperm(self.x_data.size()[0])

        # Batch data:
        # TODO: Doesn't handle incomplete last batch!!!
        x_bnf1 = torch.reshape(self.x_data[new_order], (-1, self['batch_size'], self._dims[0], 1))
        y_bnf1 = torch.reshape(self.y_data[new_order], (-1, self['batch_size'], self._dims[1], 1))
        return zip(x_bnf1, y_bnf1)