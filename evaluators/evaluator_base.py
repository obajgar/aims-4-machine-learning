from configuration import ConfigurableObject
import numpy as np


class EvaluatorBase(ConfigurableObject):
    '''
    Basis for evaluation functions such as losses or accuracies.
    '''

    def __init__(self, long_name='evaluator', **kwargs):
        super().__init__(long_name=long_name, **kwargs)
        self._values = []
        self._weights = []

    def __call__(self, y, y_pred, *args, **kwargs):
        raise NotImplementedError("Evaluator needs to implement a call function")

    def reset(self):
        self._values = []
        self._weights = []

    def partial_eval(self, y, y_pred, *args, **kwargs):
        '''
        Evaluates a batch of predictions and records them for future averaging.
        :param y:       one batch of ground truth outcomes
        :param y_pred:  one batch predictions of the model
        :param args:    other arguments to be passed to the evaluator function
        :param kwargs:  key word arguments to be passed to the evaluator function
        :return:        partial
        '''
        val = self(y, y_pred, *args, **kwargs)
        self._values.append(val.detach().numpy())
        self._weights.append(len(y))
        return val

    def report_mean(self, reset=True):
        '''
        :return: the mean of stored per-batch evaluations weighted by batch size of same
                 dim as the output of __call__ (in most cases a scalar)
        '''
        # TODO: Handle use self._weights for non-uniform batch sizes
        mean_val = np.average(self._values, weights=self._weights, axis=0)

        if reset:
            self.reset()

        return mean_val

