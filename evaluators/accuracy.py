import torch
from evaluators.evaluator_base import EvaluatorBase

class Accuracy(EvaluatorBase):
    """
    Classification accuracy calculated from logits.
    """

    def __init__(self, long_name='accuracy'):
        super().__init__(long_name=long_name)

    def __call__(self, y_bc, y_pred_bc, *args, **kwargs):
        y_pred_bc = torch.squeeze(y_pred_bc)
        y_pred_b = torch.argmax(y_pred_bc, dim=1)
        y_b = torch.argmax(y_bc, dim=1)
        accuracy = torch.mean(torch.eq(y_b,y_pred_b).float())

        return accuracy