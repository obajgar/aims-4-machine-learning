import torch
from evaluators.evaluator_base import EvaluatorBase

class MSE(EvaluatorBase):
    """
    Mean square error.
    """
    def __init__(self, long_name='mean_square_error'):
        super().__init__(long_name=long_name)

    def __call__(self, y_b, y_pred_b, *args, **kwargs):
        return torch.mean(torch.square(y_b-y_pred_b))
