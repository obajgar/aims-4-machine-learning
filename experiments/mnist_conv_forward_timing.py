from trainers import PytorchTrainer
from models import ConvolutionalModel
from datasets import MnistDataset
from evaluators import CrossEntropy, Accuracy
from optimization import SGD, Adam
from configuration import configuration

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

from c_conv import conv_torch

conv_torch.Conv.n_threads = 8

conf = {
    'trainer.epochs': 2,
    'trainer.opt.learning_rate': 0.1,
    # 'trainer.opt.lr_decay': True,

    'loss.regularization': 1e-4,

    'dataset.batch_size': 1024,
    'val_data.batch_size': 1024,

    'conv.fully_connected.activation': 'relu',
    'conv.fully_connected.last_activation': 'linear',
    'conv.fully_connected.dims': [None,10],
    'conv.n_filters': (32,),
    'conv.filter_size': (5,),
    'conv.offset': (1,),
    'conv.maxpool_size': (24,),
    'conv.maxpool_offset': (2,),
}

model = ConvolutionalModel()

train_data = MnistDataset(subset='val', flat=False)
valid_data = MnistDataset(flat=False, long_name='val_data')

trainer = PytorchTrainer(model, CrossEntropy(), train_data, Adam(),
                         extra_evaluators=[Accuracy()],
                         valid_dataset=valid_data)
configuration.add_config(conf)
trainer.run_training()

trainer.plot_training_curves()
