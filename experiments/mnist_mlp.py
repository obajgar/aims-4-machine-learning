from trainers import PytorchTrainer
from models import MLPModel
from datasets import MnistDataset
from evaluators import CrossEntropy, Accuracy
from optimization import SGD, Adam
from configuration import configuration
from result_analysis.plotting import plot_training_curves

import numpy as np
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

conf = {
    'trainer.epochs': 200,
    'trainer.opt.learning_rate': 0.1,
    'loss.regularization': 1e-4,
    'dataset.batch_size': 256,
    'model.activation': 'relu',
    'model.last_activation': 'linear'
    }

configuration.add_config(conf)

model = MLPModel([28*28, 10])

train_data = MnistDataset(flat=True)
valid_data = MnistDataset(subset='val', flat=True, long_name='val_data')

trainer = PytorchTrainer(model, CrossEntropy(), train_data, Adam(),
                         extra_evaluators=[Accuracy()],
                         valid_dataset=valid_data)

trainer.run_training()

plot_training_curves(trainer.results, trainer.all_metric_names(), index='epoch')
