from trainers import PytorchTrainer
from models import MLPModel
from datasets import MnistDataset
from evaluators import CrossEntropy, Accuracy
from optimization import SGD
import torch
from configuration import configuration
from utils import should_stop

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'


common_conf = {
    'model.activation': 'sigmoid',
    'model.dims': [28*28, 32, 32, 10],
    'trainer.opt.lr_decay': True,
    'trainer.epochs': 10000,
    'dataset.batch_size': 256
}

n_experiments = 4

param_sampling_sets = {
    'loss.regularization': [0,0.001,0.01],
    'trainer.opt.learning_rate': [0.05,0.1,0.2,0.4,0.8]
}

for n in range(n_experiments):

    if should_stop():
        print("STOP file encountered. Stopping the meta loop.")
        break

    print(f"Starting experiment # {n+1} / {n_experiments}")

    configuration.reset()
    configuration.add_config(common_conf)

    train_data = MnistDataset(flat=True)
    valid_data = MnistDataset(subset='val', flat=True, long_name='val_data')

    model = MLPModel()

    trainer = PytorchTrainer(model, CrossEntropy(), train_data, SGD(),
                             extra_evaluators=[Accuracy()],
                             valid_dataset=valid_data)

    configuration.randomly_sample(param_sampling_sets)
    trainer.run_training()

    trainer.plot_training_curves()

