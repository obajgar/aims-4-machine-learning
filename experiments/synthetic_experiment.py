from trainers import PytorchTrainer
from models import MLPModel
from datasets import SyntheticDataset
from evaluators import MSE
from optimization import SGD

import numpy as np

ground_truth_coefficient = 0.8

model = MLPModel([1, 1])
train_data = SyntheticDataset(ground_truth_coefficient)
test_data = SyntheticDataset(ground_truth_coefficient, long_name='test_data')
valid_data = SyntheticDataset(ground_truth_coefficient, long_name='valid_data')

trainer = PytorchTrainer(model, MSE(), train_data, SGD(), valid_dataset=valid_data, test_dataset=test_data)
trainer.run_training()

trainer.plot_training_curves()